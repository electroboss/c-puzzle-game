#define LWIDTH 8
#define LHEIGHT 8
#define LEVELS 6
#define LEVELBYTES 128

const unsigned char basemap[LEVELS][LEVELBYTES];
const unsigned char colmap[LEVELS][LEVELBYTES];

const unsigned char basemap[LEVELS][LEVELBYTES] = {"\
########\
########\
########\
########\
########\
########\
######&#\
########","\
########\
########\
########\
########\
########\
########\
######&#\
########","\
########\
######0#\
########\
####%%##\
###%&%##\
########\
########\
########","\
########\
######0#\
########\
####%%%#\
###%&%%#\
######%#\
##%%%%%#\
########","\
########\
##%#%%0#\
#####%##\
#%#%%%##\
###%####\
##%%##%#\
#####%&#\
########","\
########\
#####%0#\
#%###%%#\
######%#\
##%##%%#\
####%%%#\
#1#%%%&#\
########"};

const unsigned char colmap[LEVELS][LEVELBYTES] = {"\
rrrrrrrr\
r      r\
r      r\
r      r\
r      r\
r      r\
r     br\
rrrrrrrr","\
rrrrrrrr\
r r  r r\
r r  r r\
r r    r\
r r  r r\
r r  r r\
r    rbr\
rrrrrrrr","\
rrrrrrrr\
r r   yr\
r   rrrr\
r rrRR r\
r rRbR r\
r rrrr r\
r      r\
rrrrrrrr","\
rrrrrrrr\
r r   yr\
r   rrrr\
r rrRRRr\
r rRbRRr\
r rrrrRr\
r RRRRRr\
rrrrrrrr","\
rrrrrrrr\
r R RRyr\
r    R r\
rR RRR r\
r  R   r\
r RR  Rr\
r    Rbr\
rrrrrrrr","\
rrrrrrrr\
r  rrRyr\
rR r RRr\
r  r  Rr\
r R  RRr\
r   RRRr\
ryrRRRbr\
rrrrrrrr"};
