#ifndef _WIN32
#include <unistd.h>
#endif

unsigned char STOPCODE = 128;

void INThandler(int sig)
{
  signal(sig, SIG_IGN);

  STOPCODE = 0;
  stopGame();
}

unsigned char gameCompleted(void)
{
  printf("\n\033[0mGame completed!!!\n");
  STOPCODE = 1;
  stopGame();
  return 0;
}

unsigned char gameOver(void)
{
  printf("\n\033[0mGame Over!!!\n");
  STOPCODE = 1;
  stopGame();
  return 0;
}


unsigned char stopGame(void)
{
  printf("\033[0m\033[?25h\n");
  if (STOPCODE) {
#ifdef _WIN32
    Sleep(3000);
#else
    usleep(3000000);
#endif
  }
  STOP = 1;
#ifdef _WIN32
  restoreConsole();
#endif
  return 0;
}