#include "maps.h"

#define BONUSLINES 2
#ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING  0x0004
#endif

unsigned char draw(void);
unsigned char updatemap(void);
unsigned char mappos(unsigned char x);
unsigned char gameOver(void);
unsigned char gameCompleted(void);
unsigned char stopGame(void);

void INThandler(int sig);

const unsigned char defpos[2] = { 1,1 };

unsigned char currentTile;
unsigned char currentColour;

unsigned char pos[2] = { 1,1 };
unsigned char level = 0;
unsigned char STOP = 0;
unsigned char SEECRET = 0;
unsigned char p2 = 0;

float coins = 1;

unsigned char map2[72]; // LHEIGHT*(LWIDTH+1) (+1 for \n)
unsigned char colmap2[72];
unsigned char map[0x380]; // Max size needed.

unsigned char mappos(unsigned char x)
{
  return pos[0] + pos[1] * (LWIDTH + x);
}