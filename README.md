# Intro
This is a lightweight text-based video game using colored symbols in the terminal.  
I have tried to make this colourblind-friendly with the help of a friend of mine (who is red-green colourblind).
# Running
> Please note that these binaries are compiled for x86_64. You will have to build yourself if you are using a 32-bit machine. For building, see [here](#building).
## All
- For 2 player support, please download `2Player.sh` (v2.0.0+). For details on how to run in 2 player mode, please see [here](#2-player-mode).
- For v1.2.0, you will need to download `basemap.dat` and `colmap.dat`.
## UNIX
- Download `main.o` from the release that you want. 1.2.0: please place in the same directory as `colmap.dat` and `basemap.dat`.
- In the terminal run `main.o` using:
```bash
./main.o
```
- NOTE: you may have to `chmod +x main.o` to run it.
## Windows
- Download `main.exe` from the release that you want. 1.2.0: please place in the same directory as `colmap.dat` and `basemap.dat`.
- Double-click `main.exe`.
## 2 Player mode
>NOTE: The 2 Player mode server (thing) only works on linux or WSL as it is a bash script.
To run in 2 player mode (2.0.0+), you need to:
- Run `2Player.sh` with the username and ip/hostname of the local machine you're connecting to. E.G.:
```bash
./2Player.sh user@192.168.x.x
```
>NOTE: you need password-less ssh access to the maching you're connecting to. Details on how to do this can be found [here](http://www.linuxproblem.org/art_9.html).
- Run `main.o` or `main.exe` with 1 or more arguments like so:
```shell
./main.o anyword
main.exe -0Hx bhU
```
Please note that 2 player mode is in development and very buggy and slow.
# Building
## UNIX
### Perequisites
- [`gcc`](https://gcc.gnu.org/)
### Building
- run
```bash
gcc main.c -o bin/main.o
```
- binaries will be compiled in `bin`
## Windows
### Perequisites:
- [Visual Studio](https://visualstudio.microsoft.com/)
### Building
- in the visual studio developer command prompt, run:
```shell
cl main.c /Febin/main.exe
```
- binaries will be compiled in `bin`
# Tases
## Ones I made
They're in `Tases.txt`
## How to run/make them
Copy the TAS, paste into game. You can do the ones I did or make your own.